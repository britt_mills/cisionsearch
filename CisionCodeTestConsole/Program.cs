﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CisionCodeTest.Logic;



namespace CisionCodeTestConsole
{
    class Program
    {   
        static void Main(string[] args)
        {
            var appSettings = ConfigurationManager.AppSettings;
            var contactFilePath =appSettings.Get("ContactFilePath");
            var outletsFilePath = appSettings.Get("OutletsFilePath"); 
            Console.WriteLine("Welcome to Cision Search Term Test \r\n");
            Console.WriteLine("Let's Setup for Searching \r\n");
            var manager= new OutletContactManager();
            manager.Setup(contactFilePath,outletsFilePath);
            Console.WriteLine("We are ready to go. Enter a Search Term or Enter EXIT to exit \r\n");
            var input= Console.ReadLine();
            bool moreSearching = !string.Equals(input, "EXIT", StringComparison.CurrentCulture);
            while (moreSearching)
            {

                var contacts = manager.SearchTerm(input);
                if (contacts.Any())
                {
                    foreach (var contact in contacts)
                    {
                        Console.WriteLine("\n Contact Name: {0} {1} \r\n", contact.FirstName, contact.LastName);
                        Console.WriteLine("\n Contact Title: {0}\r\n", contact.Title);
                        Console.WriteLine("\n Outlet Name: {0}\r\n", contact.OutletName);
                        Console.WriteLine("\n Contact Profile: {0}  \r\n", contact.Profile);

                    }
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("\n Total Contacts found: {0} \r\n", contacts.Count());
                    Console.ResetColor();
                }
                else
                {
                    Console.WriteLine("No Results \r\n");

                }

                
              Console.WriteLine("Enter a Search Term or Enter EXIT to exit \r\n");
             input= Console.ReadLine();
             moreSearching = !string.Equals(input, "EXIT", StringComparison.CurrentCulture);
            }
                 
           

        }
   
    }
}
