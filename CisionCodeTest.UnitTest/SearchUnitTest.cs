﻿using System;
using CisionCodeTest.Logic;
using Lucene.Net.Analysis.Standard;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lucene.Net.Linq.Fluent;
using System.Linq;

namespace CisionCodeTest.UnitTest
{
    [TestClass]
    public class SearchUnitTest
    {
  

        [TestInitialize]
        public void Setup()
        {
          

        }
        [TestMethod]
        public void InSensitiveSearchTerms()
        {
            var map = new ClassMap<SearchTest>(Lucene.Net.Util.Version.LUCENE_30);
            map.Key(st => st.Id);
            map.Property(st => st.Text1).AnalyzeWith(new InsensitivePorterAnalyzer(Lucene.Net.Util.Version.LUCENE_30)).Analyzed().WithTermVector.PositionsAndOffsets();
            map.Property(st => st.Text2).AnalyzeWith(new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30));
            map.Score(st => st.Score);


            var testObjects = new SearchTest[] { new SearchTest { Id = 1, Text1 = "I AM IT . The one you have been waiting for.", Text2 = "NOT IT" }, new SearchTest { Id = 2, Text1 = "I am it. The one you have been waiting for.", Text2 = "Not it" }, new SearchTest { Id = 3, Text1 = "i am it", Text2 = "not it" } };

            SearchManager.CreateIndexAddDocuments(map, testObjects);

             var results= SearchManager.SearchOn(objectMap: map, filter: ((st) => st.Text1.Contains("I am it")));
             var sensitiveResults= SearchManager.SearchOn(objectMap: map, filter: ((st) => st.Text2.Contains("Not it")));

             Assert.AreEqual(3, results.Count());
             Assert.AreEqual(1, sensitiveResults.Count());






        }
        class SearchTest
        {
          
            public int Id { get; set; }
            public string Text1 { get; set; }

            public string Text2 { get; set; }

            public float Score { get; set; }

            
          
        }
    }
}
