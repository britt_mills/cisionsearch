﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;


namespace CisionCodeTest.Logic
{
    public class InsensitivePorterAnalyzer : StandardAnalyzer
    {
        readonly Lucene.Net.Util.Version _version;

        public InsensitivePorterAnalyzer(Lucene.Net.Util.Version useVersion)
            : base(useVersion)
        {
            _version = useVersion;
        }
        public override TokenStream TokenStream(string fieldName, TextReader reader)
        {


            var tokenizer = new StandardTokenizer(_version, reader);
            var standardStream = new StandardFilter(tokenizer);
            var lcStream = new LowerCaseFilter(standardStream);
            var stemStream = new PorterStemFilter(lcStream);
            var stopStream = new StopFilter(true, stemStream, StopAnalyzer.ENGLISH_STOP_WORDS_SET, true);

            return stopStream;
            
        }
        public override TokenStream ReusableTokenStream(string fieldName, TextReader reader)
        {
            return this.TokenStream(fieldName, reader);
        }

    
       
    }
}
