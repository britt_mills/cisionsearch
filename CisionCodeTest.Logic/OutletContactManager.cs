﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CisionCodeTest.Objects;
using Lucene.Net.Linq.Fluent;
using log4net;
using Newtonsoft.Json.Linq;
using System.IO;



namespace CisionCodeTest.Logic
{
    public class OutletContactManager
    {
        internal List<Outlet> Outlets { get; set; }
        internal List<Contact> Contacts { get; set; }

        readonly ClassMap<Outlet> _outletMap;
        readonly ClassMap<Contact> _contactMap;
        /// <summary>
        ///  Outlet and Contact main business logic and orchesatation class
        /// </summary>
        public OutletContactManager()
        {
            _outletMap = CreateOutletMap();

      
            _contactMap = CreateContactMap();

        }
        private ClassMap<Contact> CreateContactMap ()
        {
            var map = new ClassMap<Contact>(Lucene.Net.Util.Version.LUCENE_29);
            map.Key(c => c.Id);
            map.DocumentKey("DocumentType").WithFixedValue("Contact");
            map.Property(c => c.OutletId).NotAnalyzed().NotIndexed();
            map.Property(c => c.OutletName).NotAnalyzed().NotIndexed();
            map.Property(c => c.FirstName).Analyzed();
            map.Property(c => c.LastName).Analyzed();
            map.Property(c => c.Title).Analyzed().Stored();
            map.Property(c => c.Profile).AnalyzeWith(new InsensitivePorterAnalyzer(Lucene.Net.Util.Version.LUCENE_29)); ;
            map.Score(c => c.Score);
            return map;
        }
        private ClassMap<Outlet> CreateOutletMap()
        {
            var map = new ClassMap<Outlet>(Lucene.Net.Util.Version.LUCENE_29);
            map.Key(o => o.Id);
            map.DocumentKey("DocumentType").WithFixedValue("Outlet");
            map.Property(o => o.Name).Analyzed();
            map.Score(o => o.Score);
            return map;

        }
        /// <summary>
        ///  Parses the File for passed filepath. Assoicates it with a outlet. Adds it the manager Contacts property
        /// </summary>
        /// <param name="filepath">Path with File name</param>
        internal void ProcessContacts(string filepath)
        {
           
            JArray jsonFile = JArray.Parse(File.ReadAllText(filepath));
            
            var rawContacts = jsonFile.ToObject<IEnumerable<Contact>>().ToList();
            if (Outlets != null && Outlets.Any())
            {
                rawContacts.ToList().ForEach(c =>
                {

                    var outletMatch = Outlets.Where(o => o.Id == c.OutletId);
                    if (outletMatch.Any())
                    {
                        c.OutletName = outletMatch.Single().Name;

                        outletMatch.Single().Contacts.Add(c);

                    }
                });
            }
            Contacts = rawContacts;

        }
        /// <summary>
        /// Parses the File for passed filepath. Adds it to Manager's Outlets Property
        /// </summary>
        /// <param name="filepath">Path with File name</param>
        internal void ProcessOutlets(string filepath)
        {
            var jsonFile = JArray.Parse(File.ReadAllText(filepath));
            Outlets = jsonFile.ToObject<IEnumerable<Outlet>>().ToList();
            Outlets.ForEach(o => o.Contacts = new List<Contact>());
        }
        /// <summary>
        ///  process the files for contacts and outlets. Then adds them to the lucene index.
        /// </summary>
        /// <param name="contactsFilePath">>Path with File name</param>
        /// <param name="outletsFilePath">>Path with File name</param>
        public void Setup(string contactsFilePath, string outletsFilePath)
        {
            ProcessOutlets(outletsFilePath);
            ProcessContacts(contactsFilePath);
            SearchManager.CreateIndexAddDocuments(_contactMap, Contacts);
            SearchManager.CreateIndexAddDocuments(_outletMap, Outlets);

        }
        /// <summary>
        ///  This search the indiecs for <paramref name="term"/> Based on the given logic
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        public IEnumerable<Contact> SearchTerm(string term)
        {

            Func<Outlet, bool> outletNameSearch = (o) => o.Name.Contains(term);
            Func<Contact, bool> contactTermSearch = (c) => c.LastName == term || c.FirstName == term || c.Title == term || c.Profile.Contains(term);
            var matchedOutlets = SearchManager.SearchOn(outletNameSearch, _outletMap);
            var matchedContacts = SearchManager.SearchOn(contactTermSearch, _contactMap);
            var outletContacts = new List<Contact>();
            foreach (var outlet in matchedOutlets)
            {

                outletContacts.AddRange(Contacts.Where(c => c.OutletId == outlet.Id));


            }

            var contactResults = matchedContacts.Union(outletContacts).Distinct(new ContactKeyComparer());

            return contactResults.OrderByDescending(c=> c.Score);


        }
        /// <summary>
        /// For Contact key Comparer 
        /// </summary>
        private class ContactKeyComparer : IEqualityComparer<Contact>
        {
            public bool Equals(Contact x, Contact y)
            {
                return x.Id.Equals(y.Id);
            }

            public int GetHashCode(Contact obj)
            {
                return obj.GetHashCode();
            }
        }
    }
}
                                                                                                      