﻿using System;
using System.Collections.Generic;
using System.Linq;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.Linq;
using Lucene.Net.Linq.Fluent;
using Lucene.Net.Store;

namespace CisionCodeTest.Logic
{
    public static class SearchManager
    {
        private static readonly RAMDirectory _directory;
        private static readonly StandardAnalyzer _analyzer;
        private static readonly LuceneDataProvider _lProvider; 

        static SearchManager()
        {
            _directory = new RAMDirectory();
            _analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_29);
            _lProvider = new LuceneDataProvider(_directory,_analyzer, Lucene.Net.Util.Version.LUCENE_29,new IndexWriter(_directory,_analyzer,IndexWriter.MaxFieldLength.UNLIMITED));
        }

        public static void CreateIndexAddDocuments<T>(ClassMap<T> objectMap, IEnumerable<T> objects) where T:class,new ()
        {
            var documentMapper = objectMap.ToDocumentMapper();

          
            using (var session = _lProvider.OpenSession<T>(objectMap.ToDocumentMapper()))
            {
                foreach (var item in objects)
                {
                    session.Add(item);
                   
                }
                session.Commit();
            }

           
        }

        internal static IQueryable<T> Search<T>(ClassMap<T> objectMap) where T : class, new()
        {
          return  _lProvider.AsQueryable<T>(objectMap.ToDocumentMapper());
        }

        public static IEnumerable<T> SearchOn<T>(Func<T, bool> filter, ClassMap<T> objectMap) where T : class, new ()
        {
             IEnumerable<T> results;
             using (var session = _lProvider.OpenSession<T>(objectMap.ToDocumentMapper()))
             {
                 var queryItems = session.Query();
                 results = queryItems.Where(filter).ToList();
                 var test = queryItems.Where(c => c.AnyField() == "do not contact");
             
             }
            return results;        
           

        }
    }
}