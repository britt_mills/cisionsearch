﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CisionCodeTest.Objects
{
    public class Outlet
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public float Score { get; set; }

        public List<Contact> Contacts { get; set; }


        public string DocumentType { get; set; }
       
    }
}
